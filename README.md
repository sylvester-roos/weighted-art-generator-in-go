# weighted-art-generator

A rewrite of my [earlier exercise](https://gitlab.com/sylvester-roos/weighted_art_generator), but this time in Go! :)

This repository generates random art by combining a collection of assets.

![](img/preview.png)

## This is not meant as an NFT generator

The idea of the generated images is the same, but this was made because the idea itself is interesting to me. NFTs are a scam, don't invest in them.

## Information

Each class of items has a rarity; common, rare, or epic. Commons have a weight of 3, rare a weight of 2, and epic items have a weight of 1. This means that, if each class has the same amount of items, you are three times as likely to get a common item compared to an epic item.

## Usage

Calling `weighted-art-generator` without any flags will generate all possible combinations using a single thread.

### Flags

`weighted-art-generator` has two optional flags:

```
-concurrent
    Generate images concurrently. The default is false (i.e., images are generated sequentially).
-scarce
    Generate an artificially scarce amount of images. The default is false (i.e., all possible combinations are generated).
```

Example: `weighted-art-generator -concurrent -scarce`
