Format: `{level}_{rarity}/item_x.png`

For example, `1_0/face_1.png` would be an image at level 1 with a rarity of 0. The name of the file is irrelevant since it will be randomly chosen from the directory.

Rarity starts at 0, with 0 being the most common.

Levels start at 0. Level 0 is the base layer, higher levels go on top of lower levels
