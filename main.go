package main

import (
	"flag"
	"fmt"
	"io/fs"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

type Layer struct {
	Path   string
	Level  int
	Rarity int
	Weight int
}

type LayerGroup struct {
	Layers []Layer
}

func newLayer(path string, level int, rarity int, weight int) Layer {
	return Layer{
		Path:   path,
		Level:  level,
		Rarity: rarity,
		Weight: weight,
	}
}

const assetsRootDir = "assets"
const outputDir = "output"

const (
	CommonWeight = 3
	RareWeight   = 2
	EpicWeight   = 1
	CommonRarity = 0
	RareRarity   = 1
	EpicRarity   = 2
)

func main() {
	scarce := flag.Bool("scarce", false, "Generate an artificially scarce amount of images. The default is false (i.e., all possible combinations are generated).")
	concurrent := flag.Bool("concurrent", false, "Generate images concurrently. The default is false (i.e., images are generated sequentially).")

	flag.Parse()

	os.Remove(outputDir)
	os.Mkdir(outputDir, 0755)

	layers := getLayers()

	weightedLayers := createWeightedLayers(layers)

	layerGroups, err := createLayerGroups(weightedLayers, *scarce)
	if err != nil {
		fmt.Printf("Error creating layer groups: %v", err)
		return
	}

	err = createImagesFromLayerGroups(layerGroups, *concurrent)
	if err != nil {
		return
	}
}

func ExtractLevelAndRarity(dir string) (int, int, error) {
	parts := strings.Split(filepath.Base(dir), "_")

	if len(parts) != 2 {
		return 0, 0, fmt.Errorf("invalid directory name format: %s", dir)
	}

	level, err := strconv.Atoi(parts[0])
	if err != nil {
		return 0, 0, err
	}

	rarity, err := strconv.Atoi(parts[1])
	if err != nil {
		return 0, 0, err
	}

	return level, rarity, nil
}

func CalculateWeight(rarity int) (int, error) {
	switch rarity {
	case CommonRarity:
		return CommonWeight, nil
	case RareRarity:
		return RareWeight, nil
	case EpicRarity:
		return EpicWeight, nil
	default:
		return 0, fmt.Errorf("unexpected rarity value: %d", rarity)
	}
}

func getLayers() []Layer {
	var layers []Layer
	dirPattern := regexp.MustCompile(`\d+_\d+$`)

	err := filepath.WalkDir(assetsRootDir, func(path string, entry fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if !entry.IsDir() {
			dir := filepath.Dir(path)

			if !dirPattern.MatchString(dir) {
				return nil
			}

			level, rarity, err := ExtractLevelAndRarity(dir)
			if err != nil {
				return err
			}

			weight, err := CalculateWeight(rarity)
			if err != nil {
				return err
			}

			layer := newLayer(path, level, rarity, weight)
			layers = append(layers, layer)
		}

		return nil
	})

	if err != nil {
		fmt.Printf("Error walking the path %q: %v\n", assetsRootDir, err)
	}

	return layers
}

func createWeightedLayers(layers []Layer) []Layer {
	var weightedLayers []Layer
	for _, layer := range layers {
		// less efficient way of manifesting the weight, but a hell of a lot easier
		for i := 0; i < layer.Weight; i++ {
			weightedLayers = append(weightedLayers, layer)
		}
	}

	return weightedLayers
}

func randomLayers(layers []Layer) (LayerGroup, error) {
	maxLevel, err := getMaxLevel(layers)
	if err != nil {
		fmt.Println("Error getting max level:", err)
		return LayerGroup{}, err
	}

	var layerGroup LayerGroup
	for level := 0; level <= maxLevel; level++ {
		var layer Layer
		for {
			layer = layers[rand.Intn(len(layers))]
			if layer.Level == level {
				break
			}
		}
		layerGroup.Layers = append(layerGroup.Layers, layer)
	}

	return layerGroup, nil
}

func getMaxLevel(layers []Layer) (int, error) {
	maxLevel := -1

	for _, layer := range layers {
		if layer.Level > maxLevel {
			maxLevel = layer.Level
		}
	}
	if maxLevel == -1 {
		return 0, fmt.Errorf("no valid layers found")
	}

	return maxLevel, nil
}

func createLayerGroups(layers []Layer, scarce bool) ([]LayerGroup, error) {
	possibleCombinations, err := getPossibleCombinations(layers)
	if err != nil {
		fmt.Println("Error getting possible combinations:", err)
		return nil, err
	}

	layerGroupAmount := possibleCombinations
	if scarce {
		layerGroupAmount = possibleCombinations / CommonWeight
	}

	var layerGroups []LayerGroup
	addedGroups := make(map[string]bool)

	var totalCalls int

	for len(layerGroups) < layerGroupAmount {
		totalCalls++
		layerGroup, err := randomLayers(layers)
		if err != nil {
			fmt.Println("Error generating random layers:", err)
			return nil, err
		}

		// Convert the LayerGroup to a string to easily check for duplicates
		layerGroupStr := fmt.Sprint(layerGroup.Layers)

		if !addedGroups[layerGroupStr] {
			layerGroups = append(layerGroups, layerGroup)
			addedGroups[layerGroupStr] = true
		}
	}

	fmt.Printf("Images: %d\n", len(layerGroups))
	fmt.Printf("Total tries: %d\n", totalCalls)

	return layerGroups, nil
}

func getPossibleCombinations(layers []Layer) (int, error) {
	maxLevel, err := getMaxLevel(layers)
	if err != nil {
		fmt.Println("Error getting max level:", err)
		return 0, err
	}

	total := 1
	for level := 0; level <= maxLevel; level++ {
		if level != 0 {
			pattern := "assets/" + fmt.Sprint(level) + "*/*.png"
			files, err := filepath.Glob(pattern)
			if err != nil {
				fmt.Println("Error matching pattern:", err)
				return 0, err
			}
			total *= len(files)
		} else {
			total *= 1
		}
	}

	return total, nil
}

func createImagesFromLayerGroups(layerGroups []LayerGroup, concurrent bool) error {
	var wg sync.WaitGroup
	done := make(chan error, len(layerGroups))

	for i := 0; i < len(layerGroups); i++ {
		outputFilename := fmt.Sprintf("%s/%d.png", outputDir, i)
		processGroup := func(i int) {
			err := createImage(layerGroups[i], outputFilename)
			if err != nil {
				fmt.Printf("Error creating image: %v", err)
			}
			done <- err
		}

		if concurrent {
			wg.Add(1)
			go func(i int) {
				defer wg.Done()
				processGroup(i)
			}(i)
		} else {
			processGroup(i)
		}
	}

	if concurrent {
		wg.Wait()
		close(done)
	}

	return nil
}

func createImage(layerGroup LayerGroup, outputFilename string) error {
	cmd := exec.Command("convert", layerGroup.Layers[0].Path, layerGroup.Layers[1].Path, "-composite", outputFilename)
	if err := cmd.Run(); err != nil {
		fmt.Println("Error creating image")
		return fmt.Errorf("cmd.Run: %w", err)
	}

	for i := 2; i < len(layerGroup.Layers); i++ {
		cmd := exec.Command("convert", outputFilename, layerGroup.Layers[i].Path, "-composite", outputFilename)
		if err := cmd.Run(); err != nil {
			fmt.Println("Error creating image")
			return fmt.Errorf("cmd.Run: %w", err)
		}
	}

	cmd = exec.Command("convert", outputFilename, "-filter", "point", "-resize", "512x", outputFilename)
	if err := cmd.Run(); err != nil {
		fmt.Println("Error creating image")
		return fmt.Errorf("cmd.Run: %w", err)
	}

	fmt.Println("Created image:", outputFilename)

	return nil
}
